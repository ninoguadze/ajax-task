const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
    entry:"./src/index.js",
    output:{
        filename:"main.js",
        path: path.resolve(__dirname,"dist")
    },
    plugins:[new HtmlWebpackPlugin(
        {
            template: './src/index.html'
          }
    )],
    mode: 'none',
    devServer: {
      proxy: {
        '/subscribe': {
          target: 'http://localhost:3000',
          changeOrigin: true,
        },
        '/community':{
          target: 'http://localhost:3000',
          changeOrigin: true,
        },
        '/unsubscribe':{
          target: 'http://localhost:3000',
          changeOrigin: true,
        }
      },

      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
        "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
      },

        contentBase: path.join(__dirname, "dist"),
        port: 9000,
        hot: true,
    },
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: ["style-loader", "css-loader"],
        },
  
        {
          test: /\.(png|jpe?g|gif)$/i,
          type: 'asset/resource',
  
        },
      ],
    },
  };