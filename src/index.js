import {section } from './join-us-section.js';
import "../style/style.css";
import "../style/normalize.css";
import { fetchCommunityInfo } from './server-connection.js';
import logoHeader from '../assets/images/your-logo-here.png';
import logoFooter from '../assets/images/your-logo-footer.png';

document.addEventListener('DOMContentLoaded', function() {
    let sectionCreator = section("standard");
    document.getElementsByClassName("app-section app-section--image-culture")[0].after(sectionCreator); // eslint-disable-line max-len
    document.getElementsByClassName("app-logo")[0].src=logoHeader;
    document.getElementsByClassName("app-logo")[1].src=logoFooter;
   fetchCommunityInfo();
});
