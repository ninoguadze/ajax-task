function createPersonElement(person) {
    const container = document.createElement('div');
    container.setAttribute("id", person.id);
    const img = document.createElement('img');
    img.src = person.avatar;
    img.alt = person.firstName + ' ' + person.lastName;

    const header = document.createElement('h3');
    header.textContent = person.firstName + ' ' + person.lastName;

    const position = document.createElement('p');
    position.textContent = person.position;

    container.appendChild(img);
    container.appendChild(header);
    container.appendChild(position);
    container.className = "person-displayer"
    return container;
}

export function communitySectionCreator(data) {
    const section = document.createElement('section');
    section.className = "app-section community-section"
    const h1 = document.createElement('h1');
    h1.textContent = 'Big Community of People Like You';
    h1.className = 'app-title';
    const h2 = document.createElement('h2');
    h2.textContent = "We’re proud of our products, and we’re really excited when we get feedback from our users.";
    h2.className = 'app-subtitle';
    const container = document.createElement('div');
    container.className = "people-displayer"

    data.forEach(function (person) {
        container.appendChild(createPersonElement(person));
    });

    section.appendChild(h1);
    section.appendChild(h2);
    section.appendChild(container);
    document.getElementById('second-section').after(section);
}