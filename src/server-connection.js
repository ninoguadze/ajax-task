import { communitySectionCreator } from "./community-section";
import { disableButton } from "./email-validator";
export function sendEmailToServer(email) {
    disableButton(true);
    const endpoint = 'http://localhost:9000/subscribe';
    const xhr = new XMLHttpRequest();
    const data = {
        email: email
    };
    xhr.open('POST', endpoint, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(data));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                console.log('Email successfully sent to the server.');
            }
            else if (xhr.status === 422) {
                window.alert(xhr.response)
            }
            else {
                console.error('Failed to send email to the server.');
            }
        }
    };
    setTimeout(function () {
        disableButton(false)
    }, 1000)
};

export function unsubscribeFromServer(email) {
    disableButton(true);
    const endpoint = 'http://localhost:9000/unsubscribe';
    const xhr = new XMLHttpRequest();
    xhr.open('POST', endpoint, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    const data = {
        email: email
    };
    xhr.send(JSON.stringify(data));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                console.log('Successfully unsubscribed.');
            } else {
                console.error('Failed to unsubscribe. Please try again later.');
            }
        }
    };
    setTimeout(function () {
        disableButton(false)
    }, 1000);
};

export function fetchCommunityInfo() {
    const endpoint = 'http://localhost:9000/community';
    const xhr = new XMLHttpRequest();
    xhr.open('GET', endpoint, true);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                const communityData = JSON.parse(xhr.responseText);
                console.log('Succesfully returned data');
                communitySectionCreator(communityData);
                return communityData;
            } else {
                console.error('Failed to fetch community information.');
                return {};
            }
        }
    };
};

